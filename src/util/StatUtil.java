package util;

import csgo.MapResult;
import csgo.Match;
import csgo.PredictionData;
import csgo.Team;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StatUtil {

    private final static String chromeDriverPath = "D:/User Folders/Desktop/chromedriver.exe";

    public PredictionData grabData(String url) {
        System.setProperty("webdriver.chrome.driver", chromeDriverPath);

        WebDriver driver = new ChromeDriver();
        driver.manage().deleteAllCookies();

        driver.get(url);

        List<WebElement> texts = ((ChromeDriver) driver).findElementsByClassName("teamName");
        String tName1 = texts.get(0).getText();
        String tName2 = texts.get(1).getText();
        List<String> players1 = new ArrayList<>();
        List<String> players2 = new ArrayList<>();

        for(int i = 0; i < 5; i++) {
            WebElement nameElement1 = ((ChromeDriver) driver).findElementByXPath("/html/body/div[2]/div/div[2]/div[1]/div[1]/div[10]/div/div[1]/div[2]/table/tbody/tr[2]/td[" + (i + 1) + "]/a/div/div");
            WebElement nameElement2 = ((ChromeDriver) driver).findElementByXPath("/html/body/div[2]/div/div[2]/div[1]/div[1]/div[10]/div/div[2]/div[2]/table/tbody/tr[2]/td[" + (i + 1) + "]/a/div/div");

            players1.add(nameElement1.getText());
            players2.add(nameElement2.getText());
        }

        Team team1 = new Team(tName1, players1);
        Team team2 = new Team(tName2, players2);

        List<Match> matches1 = new ArrayList<>();
        List<Match> matches2 = new ArrayList<>();
        List<Match> matches = new ArrayList<>();

        WebElement mapNameBox = ((ChromeDriver) driver).findElement(By.className("mapholder")).findElement(By.className("mapname"));
        String mapToBePlayed = mapNameBox.getText();

        //Start of match grabbing
        for(int x = 1; x < 4; x += 2) {
            WebElement teamBox = ((ChromeDriver) driver).findElementByXPath("/html/body/div[2]/div/div[2]/div[1]/div[1]/div[1]/div[" + x + "]/div/a");
            teamBox.click();

            String teamId = driver.getCurrentUrl().split("team/")[1].split("/")[0];

            driver.get("https://www.hltv.org/results?team=" + teamId);

            List<WebElement> matchBoxGroups;

            if(driver.findElements(By.className("results-all")).size() == 1) {
                matchBoxGroups = driver.findElement(By.className("results-all")).findElements(By.className("results-sublist"));
            } else {
                matchBoxGroups = driver.findElements(By.className("results-all")).get(1).findElements(By.className("results-sublist"));
                if (matchBoxGroups.size() < 4) {
                    matchBoxGroups = driver.findElements(By.className("results-all")).get(2).findElements(By.className("results-sublist"));
                }
            }

            for (int i = 0; i < matchBoxGroups.size(); i++) {
                WebElement anchor;

                try {
                    anchor = matchBoxGroups.get(i).findElement(By.className("standard-headline"));
                } catch (NoSuchElementException e) {
                    System.out.println("Thrown");
                    continue;
                }

                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMMM d yyyy");
                String rawDate = anchor.getText();

                rawDate = rawDate.substring(12);
                rawDate = rawDate.replace("th", "")
                        .replace("st", "")
                        .replace("rd", "")
                        .replace("nd", "");

                if (rawDate.contains("Augu")) rawDate = rawDate.replace("Augu", "August");

                LocalDate date = LocalDate.parse(rawDate, formatter);
                long daysSince = ChronoUnit.DAYS.between(date, LocalDate.now());

                if (daysSince > 60) break;

                List<WebElement> matchBoxes = matchBoxGroups.get(i).findElements(By.className("a-reset"));

                matchloop:
                for (int j = 0; j < matchBoxes.size(); j++) {
                    String href = matchBoxes.get(j).getAttribute("href");

                    ((JavascriptExecutor) driver).executeScript("window.open()");

                    List<String> tabs = new ArrayList<>(driver.getWindowHandles());
                    driver.switchTo().window(tabs.get(tabs.size() - 1));

                    driver.get(href);

                    String matchTeam1 = driver.findElements(By.className("teamName")).get(0).getText();
                    String matchTeam2 = driver.findElements(By.className("teamName")).get(1).getText();

                    if(!matchTeam1.equals(team1.getName()) && !matchTeam1.equals(team2.getName())
                            && !matchTeam2.equals(team1.getName()) && !matchTeam2.equals(team2.getName())) {
                        driver.close();
                        driver.switchTo().window(tabs.get(0));
                        continue;
                    }

                    List<String> teamPlayers1 = new ArrayList<>();
                    List<String> teamPlayers2 = new ArrayList<>();
                    Map<String, Double> ratings = new HashMap<>();

                    for (int table = 0; table < 2; table++) {
                        for (int row = 0; row < 5; row++) {
                            WebElement tableRow;
                            try {
                                tableRow = driver.findElement(By.xpath("//*[@id=\"all-content\"]/table[" + (table + 1) + "]/tbody/tr[" + (row + 2) + "]"));
                            } catch (NoSuchElementException e) {
                                driver.close();
                                driver.switchTo().window(tabs.get(0));

                                continue matchloop;
                            }
                            //Grab HLTV Ratings, populate player lists
                            String playerName = tableRow.findElement(By.className("player-nick")).getText();
                            double rating = Double.valueOf(tableRow.findElement(By.className("rating")).getText());
                            ratings.put(playerName, rating);

                            switch (table) {
                                case 0:
                                    teamPlayers1.add(playerName);
                                    break;
                                case 1:
                                    teamPlayers2.add(playerName);
                            }
                        }
                    }

                    Team teamA = new Team(matchTeam1, teamPlayers1);
                    Team teamB = new Team(matchTeam2, teamPlayers2);

                    WebElement infoBox = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div"));
                    String info = infoBox.getText();

                    List<MapResult> maps = new ArrayList<>();

                    //Retrieve score
                    if (info.startsWith("Best of 3") || info.startsWith("Best of 5")) {
                        WebElement mapsBox = driver.findElement(By.className("flexbox-column"));
                        List<WebElement> mapBoxes = mapsBox.findElements(By.className("results"));
                        List<WebElement> upperMapBoxes = mapsBox.findElements(By.className("mapname"));

                        assert mapBoxes.size() == upperMapBoxes.size();

                        for (int index = 0; index < mapBoxes.size(); index++) {
                            String scoresRaw = mapBoxes.get(index).getText().split(" ")[0];
                            int scoreA = Integer.valueOf(scoresRaw.split(":")[0]);
                            int scoreB = Integer.valueOf(scoresRaw.split(":")[1]);

                            String map = upperMapBoxes.get(index).getText().split("\n")[0];
                            MapResult mapResult = new MapResult(scoreA, scoreB, map);
                            maps.add(mapResult);
                        }
                    } else {
                        WebElement scoreTextA = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[1]/div[1]/div[1]/div[1]/div/div"));
                        WebElement scoreTextB = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[1]/div[1]/div[1]/div[3]/div/div"));
                        int scoreA = Integer.valueOf(scoreTextA.getText());
                        int scoreB = Integer.valueOf(scoreTextB.getText());
                        WebElement mapBox = driver.findElement(By.className("mapholder"));
                        String mapName = mapBox.getText().split("\n")[0];
                        MapResult mapResult = new MapResult(scoreA, scoreB, mapName);
                        maps.add(mapResult);
                    }

                    Match match = new Match(teamA, teamB, ratings, date, maps);

                    if(match.hasTeam(team1) && match.hasTeam(team2)) {
                        driver.close();
                        driver.switchTo().window(tabs.get(0));
                        continue;
                    }

                    int pCount = 0;
                    Team team;
                    if(match.hasTeam(team1))
                        team = team1;
                    else
                        team = team2;

                    for(String player : team.getPlayers()) {
                        if(match.getRatings().containsKey(player)) pCount++;
                    }

                    if(pCount < 4) {
                        driver.close();
                        driver.switchTo().window(tabs.get(0));
                        continue;
                    }

                    switch (x) {
                        case 1:
                            matches1.add(match);
                            break;
                        case 3:
                            matches2.add(match);
                    }

                    driver.close();
                    driver.switchTo().window(tabs.get(0));
                }
            }

            driver.get(url);
        }

        for(Match match : matches1) {
            Team other = match.getOtherTeam(team1);

            for(Match oMatch : matches2) {
                if(oMatch.hasTeam(other)) {
                    if (!matches.contains(oMatch))
                        matches.add(oMatch);

                    if(!matches.contains(match))
                        matches.add(match);
                }
            }
        }

        driver.quit();

        return new PredictionData(team1, team2, matches, mapToBePlayed);
    }

}
