package util;

import java.util.List;

public class Util {

    public static double[] intListToDoubleArr(List<Integer> list) {
        double[] vals = new double[list.size()];
        for(int i = 0; i < list.size(); i++) {
            vals[i] = list.get(i);
        }

        return vals;
    }

    public static double getSum(double[] a) {
        double sum = 0;
        for(double num : a)
            sum += num;
        return sum;
    }

}
