package util;

import csgo.MapResult;
import csgo.Match;
import csgo.PredictionData;
import csgo.Team;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.stat.ranking.NaNStrategy;
import org.apache.commons.math3.stat.ranking.NaturalRanking;
import org.apache.commons.math3.stat.ranking.TiesStrategy;
import org.apache.commons.math3.util.FastMath;

import java.time.LocalDate;
import java.time.Period;
import java.util.*;

public class DataUtil {

    public double getTeamMatrixStat(PredictionData data) {
        Map<String, List<Double>> teamsA = new HashMap<>();
        Map<String, List<Double>> teamsB = new HashMap<>();
        Map<String, List<LocalDate>> teamDatesA = new HashMap<>();
        Map<String, List<LocalDate>> teamDatesB = new HashMap<>();

        for(Match match : data.getMatches()) {
            Team team;
            Map<String, List<Double>> teams;
            Map<String, List<LocalDate>> teamDates;

            if(match.hasTeam(data.getTeamA())) {
                team = data.getTeamA();
                teams = teamsA;
                teamDates = teamDatesA;
            } else {
                team = data.getTeamB();
                teams = teamsB;
                teamDates = teamDatesB;
            }

            Team otherTeam = match.getOtherTeam(team);

            for(MapResult result : match.getMaps()) {
                int score1, score2;

                if(match.getTeamA().getName().equals(team.getName())) {
                    score1 = result.getScoreA();
                    score2 = result.getScoreB();
                } else {
                    score1 = result.getScoreB();
                    score2 = result.getScoreA();
                }

                List<Double> mapsWon = teams.getOrDefault(otherTeam.getName(), new ArrayList<>());
                mapsWon.add((double) score1 - score2);
                teams.put(otherTeam.getName(), mapsWon);

                List<LocalDate> dates = teamDates.getOrDefault(otherTeam.getName(), new ArrayList<>());
                dates.add(match.getDate());
                teamDates.put(otherTeam.getName(), dates);
            }
        }

        double pSum = 0;
        int pCount = 0;

        for(String otherTeam : teamsA.keySet()) {
            if(teamsB.containsKey(otherTeam)) {
                int minDaysSince = 0; //earliest date days since
                int maxDaysSince = 0; //latest date days since
                for(LocalDate date : teamDatesA.get(otherTeam)) {
                    int daysSince = Period.between(date, LocalDate.now()).getDays();
                    if(minDaysSince == 0 || daysSince > minDaysSince)
                        minDaysSince = daysSince;
                    if(maxDaysSince == 0 || daysSince < maxDaysSince)
                        maxDaysSince = daysSince;
                }
                for(LocalDate date : teamDatesB.get(otherTeam)) {
                    int daysSince = Period.between(date, LocalDate.now()).getDays();
                    if(minDaysSince == 0 || daysSince > minDaysSince)
                        minDaysSince = daysSince;
                    if(maxDaysSince == 0 || daysSince < maxDaysSince)
                        maxDaysSince = daysSince;
                }

                double dateFactor = Math.pow((60 - minDaysSince) / 60.0, 2);
                double dateDiffFactor = Math.pow((60 - (minDaysSince - maxDaysSince)) / 60.0, 2);

                double[] teamAPerf = teamsA.get(otherTeam).stream().mapToDouble(Double::doubleValue).toArray();
                double[] teamBPerf = teamsB.get(otherTeam).stream().mapToDouble(Double::doubleValue).toArray();

                double[] combined = new double[teamAPerf.length + teamBPerf.length];
                System.arraycopy(teamAPerf, 0, combined, 0, teamAPerf.length);
                System.arraycopy(teamBPerf, 0, combined, teamAPerf.length, teamBPerf.length);

                NaturalRanking natRanking = new NaturalRanking(NaNStrategy.FIXED, TiesStrategy.AVERAGE);
                double[] ranked = natRanking.rank(combined);
                double sumRankX = 0;

                for(int i = 0; i < teamAPerf.length; ++i) {
                    sumRankX += ranked[i];
                }

                double U1 = sumRankX - (double)((long)teamAPerf.length * (long)(teamAPerf.length + 1) / 2L);
                double U2 = (double)((long)teamAPerf.length * (long)teamBPerf.length) - U1;

                boolean isTeamB = U2 > U1;

                double U_stat = Math.max(U1, U2);
                double p = calcP(U_stat, teamAPerf.length, teamBPerf.length);
                p = isTeamB ? -p : p;

                pSum += p * dateFactor * dateDiffFactor;
                pCount++;
            }
        }

        return pSum / pCount;
}

    public double getMapMatrixStat(PredictionData data) {
        Map<String, List<Integer>> mapsA = new HashMap<>();
        Map<String, List<Integer>> mapsB = new HashMap<>();
        Map<String, List<LocalDate>> mapDatesA = new HashMap<>();
        Map<String, List<LocalDate>> mapDatesB = new HashMap<>();

        for(Match match : data.getMatches()) {
            Team team;
            Map<String, List<Integer>> maps;
            Map<String, List<LocalDate>> mapDates;

            if(match.hasTeam(data.getTeamA())) {
                maps = mapsA;
                team = data.getTeamA();
                mapDates = mapDatesA;
            } else {
                maps = mapsB;
                team = data.getTeamB();
                mapDates = mapDatesB;
            }

            for(MapResult result : match.getMaps()) {
                int differential;

                if(match.getTeamA().getName().equals(team.getName()))
                    differential = result.getScoreA() - result.getScoreB();
                else
                    differential = result.getScoreB() - result.getScoreA();

                List<Integer> diffs = maps.getOrDefault(result.getMapName(), new ArrayList<>());
                diffs.add(differential);
                maps.put(result.getMapName(), diffs);

                List<LocalDate> dates = mapDates.getOrDefault(result.getMapName(), new ArrayList<>());
                dates.add(match.getDate());
                mapDates.put(result.getMapName(), dates);
            }
        }

        double pSum = 0;
        int pCount = 0;

        for (String mapName : mapsA.keySet()) {
            if (mapsB.containsKey(mapName)) {
                int minDaysSince = -1; //earliest date days since
                int maxDaysSince = -1; //latest date days since
                for (LocalDate date : mapDatesA.get(mapName)) {
                    int daysSince = Period.between(date, LocalDate.now()).getDays();
                    if (minDaysSince == -1 || daysSince > minDaysSince)
                        minDaysSince = daysSince;
                    if (maxDaysSince == -1 || daysSince < maxDaysSince)
                        maxDaysSince = daysSince;
                }
                for (LocalDate date : mapDatesB.get(mapName)) {
                    int daysSince = Period.between(date, LocalDate.now()).getDays();
                    if (minDaysSince == -1 || daysSince > minDaysSince)
                        minDaysSince = daysSince;
                    if (maxDaysSince == -1 || daysSince < maxDaysSince)
                        maxDaysSince = daysSince;
                }

                double dateFactor = Math.pow((60 - minDaysSince) / 60.0, 2);
                double dateDiffFactor = Math.pow((60 - (minDaysSince - maxDaysSince)) / 60.0, 2);

                double[] diffsA = Util.intListToDoubleArr(mapsA.get(mapName));
                double[] diffsB = Util.intListToDoubleArr(mapsB.get(mapName));

                double[] combined = new double[diffsA.length + diffsB.length];
                System.arraycopy(diffsA, 0, combined, 0, diffsA.length);
                System.arraycopy(diffsB, 0, combined, diffsA.length, diffsB.length);

                NaturalRanking natRanking = new NaturalRanking(NaNStrategy.FIXED, TiesStrategy.AVERAGE);
                double[] ranked = natRanking.rank(combined);
                double sumRankX = 0;

                for (int i = 0; i < diffsA.length; ++i) {
                    sumRankX += ranked[i];
                }

                double U1 = sumRankX - (double) ((long) diffsA.length * (long) (diffsA.length + 1) / 2L);
                double U2 = (double) ((long) diffsA.length * (long) diffsB.length) - U1;

                boolean isTeamB = U2 > U1;

                double U_stat = Math.max(U1, U2);
                double p = calcP(U_stat, diffsA.length, diffsB.length);

                double p2 = 1 - p;
                double diffFactor = Math.pow(1 - Math.abs(p - p2), 2);

                p = isTeamB ? -p : p;

                pSum += p * dateFactor * diffFactor * dateDiffFactor;
                pCount++;
            }
        }

        return pSum / pCount;
    }

    public int getNumMatch(PredictionData data) {
        return data.getMatches().size();
    }

    public double getHLTVStat(PredictionData data) {
        Map<String, List<Double>> ratingsA = new HashMap<>();
        Map<String, List<Double>> ratingsB = new HashMap<>();
        Map<String, List<LocalDate>> teamDatesA = new HashMap<>();
        Map<String, List<LocalDate>> teamDatesB = new HashMap<>();

        for(Match match : data.getMatches()) {
            Team team;
            Map<String, List<Double>> ratings;
            Map<String, List<LocalDate>> teamDates;
            List<String> roster;

            if(match.hasTeam(data.getTeamA())) {
                team = data.getTeamA();
                ratings = ratingsA;
                teamDates = teamDatesA;
                roster = data.getTeamA().getPlayers();
            } else {
                team = data.getTeamB();
                ratings = ratingsB;
                teamDates = teamDatesB;
                roster = data.getTeamB().getPlayers();
            }

            Team otherTeam = match.getOtherTeam(team);

            List<Double> matchRatings = ratings.getOrDefault(otherTeam.getName(), new ArrayList<>());

            for(String player : match.getRatings().keySet())
                if(roster.contains(player))
                    matchRatings.add(match.getRatings().get(player));

            ratings.put(otherTeam.getName(), matchRatings);

            List<LocalDate> dates = teamDates.getOrDefault(otherTeam.getName(), new ArrayList<>());
            dates.add(match.getDate());
            teamDates.put(otherTeam.getName(), dates);
        }

        double pSum = 0;
        int pCount = 0;

        for(String otherTeam : ratingsA.keySet()) {
            if(ratingsB.containsKey(otherTeam)) {
                int minDaysSince = 0; //earliest date days since
                int maxDaysSince = 0; //latest date days since
                for(LocalDate date : teamDatesA.get(otherTeam)) {
                    int daysSince = Period.between(date, LocalDate.now()).getDays();
                    if(minDaysSince == 0 || daysSince > minDaysSince)
                        minDaysSince = daysSince;
                    if(maxDaysSince == 0 || daysSince < maxDaysSince)
                        maxDaysSince = daysSince;
                }
                for(LocalDate date : teamDatesB.get(otherTeam)) {
                    int daysSince = Period.between(date, LocalDate.now()).getDays();
                    if(minDaysSince == 0 || daysSince > minDaysSince)
                        minDaysSince = daysSince;
                    if(maxDaysSince == 0 || daysSince < maxDaysSince)
                        maxDaysSince = daysSince;
                }

                double dateFactor = Math.pow((60 - minDaysSince) / 60.0, 2);
                double dateDiffFactor = Math.pow((60 - (minDaysSince - maxDaysSince)) / 60.0, 2);

                double[] teamAPerf = ratingsA.get(otherTeam).stream().mapToDouble(Double::doubleValue).toArray();
                double[] teamBPerf = ratingsB.get(otherTeam).stream().mapToDouble(Double::doubleValue).toArray();

                double[] combined = new double[teamAPerf.length + teamBPerf.length];
                System.arraycopy(teamAPerf, 0, combined, 0, teamAPerf.length);
                System.arraycopy(teamBPerf, 0, combined, teamAPerf.length, teamBPerf.length);

                NaturalRanking natRanking = new NaturalRanking(NaNStrategy.FIXED, TiesStrategy.AVERAGE);
                double[] ranked = natRanking.rank(combined);
                double sumRankX = 0;

                for(int i = 0; i < teamAPerf.length; ++i) {
                    sumRankX += ranked[i];
                }

                double U1 = sumRankX - (double)((long)teamAPerf.length * (long)(teamAPerf.length + 1) / 2L);
                double U2 = (double)((long)teamAPerf.length * (long)teamBPerf.length) - U1;

                boolean isTeamB = U2 > U1;

                double U_stat = Math.max(U1, U2);
                double p = calcP(U_stat, teamAPerf.length, teamBPerf.length);
                p = isTeamB ? -p : p;

                pSum += p * dateFactor * dateDiffFactor;
                pCount++;
            }
        }

        return pSum / pCount;
    }

    private double calcP(double U_stat, int n1, int n2) {
        long n1n2prod = (long) n1 * (long) n2;
        double EU = n1n2prod / 2.0D;
        double VarU = (double)(n1n2prod * (long)(n1 + n2 + 1)) / 12.0D;
        double z = (U_stat - EU) / FastMath.sqrt(VarU);
        NormalDistribution standardNormal = new NormalDistribution(null, 0.0D, 1.0D);
        double p = standardNormal.cumulativeProbability(z);
        return p;
    }

}
