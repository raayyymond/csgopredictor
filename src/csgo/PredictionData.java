package csgo;

import java.util.List;

public class PredictionData {

    Team teamA, teamB;
    List<Match> matches;
    String map;

    public PredictionData() {
    }

    public PredictionData(Team teamA, Team teamB, List<Match> matches, String map) {
        this.teamA = teamA;
        this.teamB = teamB;
        this.matches = matches;
        this.map = map;
    }

    public Team getTeamA() {
        return teamA;
    }

    public void setTeamA(Team teamA) {
        this.teamA = teamA;
    }

    public Team getTeamB() {
        return teamB;
    }

    public void setTeamB(Team teamB) {
        this.teamB = teamB;
    }

    public List<Match> getMatches() {
        return matches;
    }

    public void setMatches(List<Match> matches) {
        this.matches = matches;
    }

    public String getMap() {
        return map;
    }

    public void setMap(String map) {
        this.map = map;
    }
}
