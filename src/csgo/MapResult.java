package csgo;

public class MapResult {

    private int scoreA, scoreB;
    private String mapName;

    public MapResult() {
    }

    public MapResult(int scoreA, int scoreB, String mapName) {
        this.scoreA = scoreA;
        this.scoreB = scoreB;
        this.mapName = mapName;
    }

    public int getScoreA() {
        return scoreA;
    }

    public int getScoreB() {
        return scoreB;
    }

    public String getMapName() {
        return mapName;
    }

    public void setScoreA(int scoreA) {
        this.scoreA = scoreA;
    }

    public void setScoreB(int scoreB) {
        this.scoreB = scoreB;
    }

    public void setMapName(String mapName) {
        this.mapName = mapName;
    }
}
