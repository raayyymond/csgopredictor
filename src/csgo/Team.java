package csgo;

import java.util.List;

public class Team {

    private String name;
    private List<String> players;

    public Team() {
    }

    public Team(String name, List<String> players) {
        this.name = name;
        this.players = players;
    }

    public String getName() {
        return name;
    }

    public List<String> getPlayers() {
        return players;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPlayers(List<String> players) {
        this.players = players;
    }

    @Override
    public String toString() {
        return "Name: " + name + "; Players: " + players ;
    }
}
