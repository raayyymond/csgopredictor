package csgo;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

public class Match {

    private Team teamA, teamB;
    private Map<String, Double> ratings; // player name : HLTV Rating 2.0
    private LocalDate date;
    private List<MapResult> maps;

    public Match() {
    }

    public Match(Team teamA, Team teamB, Map<String, Double> ratings, LocalDate date, List<MapResult> maps) {
        this.teamA = teamA;
        this.teamB = teamB;
        this.ratings = ratings;
        this.date = date;
        this.maps = maps;
    }

    public boolean hasTeam(Team team) {
        return teamA.getName().equals(team.getName()) || teamB.getName().equals(team.getName());
    }

    public Team getOtherTeam(Team team) {
        return teamA.getName().equals(team.getName()) ? teamB : teamA;
    }

    public Map<String, Double> getRatings() {
        return ratings;
    }

    public Team getTeamA() {
        return teamA;
    }

    public Team getTeamB() {
        return teamB;
    }

    public void setTeamA(Team teamA) {
        this.teamA = teamA;
    }

    public void setTeamB(Team teamB) {
        this.teamB = teamB;
    }

    public void setRatings(Map<String, Double> ratings) {
        this.ratings = ratings;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public List<MapResult> getMaps() {
        return maps;
    }

    public void setMaps(List<MapResult> maps) {
        this.maps = maps;
    }

    @Override
    public String toString() {
        return teamA + "   vs   " + teamB;
    }
}
