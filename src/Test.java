import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import csgo.PredictionData;
import util.StatUtil;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;

public class Test {

    public static void main(String[] args) throws IOException {

        LocalDateTime start = LocalDateTime.now();

        StatUtil statUtil = new StatUtil();

        PredictionData data = statUtil.grabData("https://www.hltv.org/matches/2328088/astralis-vs-north-esl-pro-league-season-8-europe");

        File file = new File("D:/User Folders/Desktop/matchData3.yml");

        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        try {
            mapper.writeValue(file, data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        LocalDateTime stop = LocalDateTime.now();

        System.out.println("Data Collection Time: " + Duration.between(start, stop).toMinutes() + " minutes");
    }

}
