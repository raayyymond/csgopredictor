import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import csgo.PredictionData;
import util.DataUtil;
import util.StatUtil;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

public class CSGOPredictor {

    public static void main(String[] args) throws IOException {
        LocalDateTime start = LocalDateTime.now();
        
        Scanner scanner = new Scanner(new File("D:/User Folders/Desktop/input.yml"));

        File output = new File("D:/User Folders/Desktop/dataset.yml");

        Map<String, List<Double>> generatedStats = new HashMap<>();

        StatUtil statUtil = new StatUtil();
        DataUtil dataUtil = new DataUtil();

        while(scanner.hasNextLine()) {
            String input = scanner.nextLine();

            String team1 = input.split("/")[5].split("-")[0];
            String team2 = input.split("/")[5].split("-vs-")[1].split("-")[0];

            String key = null;
            for(String mapKey : generatedStats.keySet()) {
                if(mapKey.contains(team1) && mapKey.contains(team2)) {
                    key = mapKey;
                }
            }

            double[] stats;
            if(key == null) {
                PredictionData data;

                try {
                    data = statUtil.grabData(input);
                } catch (Exception e) {
                    data = null;
                }

                if(data == null) {
                    String line = "Error";
                    line += System.lineSeparator();
                    FileWriter writer = new FileWriter(output, true);
                    writer.append(line);
                    writer.close();
                    System.out.println(line);
                    continue;
                }

                int numMatches = dataUtil.getNumMatch(data);

                if(numMatches < 30) {
                    String line = "Less than 30 matches available";
                    line += System.lineSeparator();
                    FileWriter writer = new FileWriter(output, true);
                    writer.append(line);
                    writer.close();
                    System.out.println(line);
                    continue;
                }

                LocalDate date = LocalDate.now();

                String dateString = date.getMonth().getValue() + "-" +
                        date.getDayOfMonth() + "-" + date.getYear();

                File folder = new File("D:/User Folders/Desktop/Match Data/" + dateString + "/");
                if(!folder.exists())
                    folder.mkdir();

                File file = new File("D:/User Folders/Desktop/Match Data/" + dateString + "/" + data.getTeamA().getName() + "-" + data.getTeamB().getName() + ".yml");

                if(!file.exists())
                    file.createNewFile();

                ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
                mapper.registerModule(new JavaTimeModule());
                mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
                try {
                    mapper.writeValue(file, data);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                stats = new double[] {numMatches, dataUtil.getHLTVStat(data), dataUtil.getTeamMatrixStat(data), dataUtil.getMapMatrixStat(data)};
                List<Double> statsList = Arrays.stream(stats).boxed().collect(Collectors.toList());
                generatedStats.put(team1 + team2, statsList);
            } else {
                List<Double> statsRaw = generatedStats.get(key);
                stats = statsRaw.stream().mapToDouble(Double::doubleValue).toArray();
                if(key.startsWith(team2))
                    for(int i = 1; i < stats.length; i++)
                        stats[i] = -stats[i];
            }

            String line = "";
            for(double stat : stats) {
                line += stat + ",";
            }
            line += System.lineSeparator();
            line = line.replace("Infinity", "1.0");
            FileWriter writer = new FileWriter(output, true);
            writer.append(line);
            writer.close();
            System.out.println(line);
        }

        LocalDateTime stop = LocalDateTime.now();
        System.out.println("Data Collection Time: " + Duration.between(start, stop).toMinutes() + " minutes");
    }
}
