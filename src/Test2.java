import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import csgo.PredictionData;
import util.DataUtil;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Test2 {

    public static void main(String[] args) throws IOException {
        File folder = new File("D:/User Folders/Desktop/11-7-18");
        File[] listOfFiles = folder.listFiles();

        File output = new File("D:/User Folders/Desktop/tempdataset.yml");

        for(int i = 0; i < listOfFiles.length; i++) {
            File file = listOfFiles[i];
            PredictionData data = null;
            ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
            mapper.registerModule(new JavaTimeModule());
            mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
            try {
                data = mapper.readValue(file, PredictionData.class);
            } catch (Exception e) {
                e.printStackTrace();
            }

            DataUtil dataUtil = new DataUtil();

            String line = dataUtil.getMapMatrixStat(data) + System.lineSeparator();
            FileWriter writer = new FileWriter(output, true);
            writer.append(line);
            writer.close();
            System.out.println(file.getName());
            System.out.println(line);
        }
    }

}
