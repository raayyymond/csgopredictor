import org.apache.commons.math3.distribution.NormalDistribution;

public class Test4 {

    public static void main(String[] args) {
        NormalDistribution standardNormal = new NormalDistribution(null, 0.0D, 1.0D);
        double p = standardNormal.probability(0, 1);
        System.out.println("p = " + p);
    }

}
